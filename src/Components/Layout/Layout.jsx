import React from 'react'
import Footer from './Footer/Footer'
import Header from './Header/Header'
import './style.css'

export default function Layout() {
    return (

        <>
            <body id="home" className="io-false isDesktop isChrome" data-new-gr-c-s-check-loaded="14.1087.0" data-gr-ext-installed="" cz-shortcut-listen="true">
                <div id="wrapper" className="visible">
                    <Header />
                    <div className="section">
                        <div className="background" data-desktop="/assets/images/backgrounds/Homepage_Desktop.png" data-mobile="/assets/images/backgrounds/Homepage_Mobile.png" style={{ backgroundImage: 'url(https://www.spacex.com/static/images/backgrounds-2022/crs-26/pre-launch/Homepage_Desktop.webp)' }}></div>
                        <div className="section-inner feature" style={{ height: '655px' }}>
                            <div className="inner-left-bottom">

                                <h4 className="animate" style={{ textTransform: 'uppercase' }}>Upcoming Launch</h4>


                                <h2 className="animate shadowed">CRS-26 Mission</h2>

                                <a className="btn animate" tabIndex="0" href="/launches/crs-26/">
                                    <div className="hover"></div>
                                    <span className="text">WATCH</span>
                                </a>

                            </div>
                            <div className="scrollme" style={{ visibility: 'inherit', opacity: '0.995595', transform: 'translate3d(0px, -38.3353px, 0px)' }}>
                                <svg width="30px" height="20px">
                                    <path stroke="#ffffff" strokeWidth="2px" d="M2.000,5.000 L15.000,18.000 L28.000,5.000 "></path>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div className="section">
                        <div className="background" data-desktop="/static/images/backgrounds-2022/eutelsat-10b/post-launch/Homepage_Desktop.jpg" data-mobile="/static/images/backgrounds-2022/eutelsat-10b/post-launch/Homepage_Mobile.jpg" style={{ backgroundImage: 'url(https://www.spacex.com/static/images/backgrounds-2022/eutelsat-10b/post-launch/Homepage_Desktop.webp)' }}></div>
                        <div className="section-inner feature" style={{ height: '655px' }}>
                            <div className="inner-left-bottom">

                                <h4 className="animate" style={{ textTransform: 'uppercase' }}>Recent Launch</h4>


                                <h2 className="animate shadowed">Eutelsat 10B Mission</h2>

                                <a className="btn animate" tabIndex="0" href="/launches/eutelsat-10b/">
                                    <div className="hover"></div>
                                    <span className="text">REWATCH</span>
                                </a>

                            </div>
                            <div className="scrollme" style={{ visibility: 'inherit', opacity: '0.995595', transform: 'translate3d(0px, -38.3353px, 0px)' }}>
                                <svg width="30px" height="20px">
                                    <path stroke="#ffffff" strokeWidth="2px" d="M2.000,5.000 L15.000,18.000 L28.000,5.000 "></path>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div className="section">
                        <div className="background" data-desktop="/static/images/backgrounds-2021/hls-resized-2.jpg" data-mobile="/static/images/backgrounds-2021/HLS_Mobile.jpg" style={{ backgroundImage: 'url(https://www.spacex.com/static/images/backgrounds-2021/hls-resized-2.webp)' }}></div>
                        <div className="section-inner feature" style={{ height: '655px' }}>
                            <div className="inner-left-bottom">


                                <h2 className="animate shadowed">Starship Selected by NASA to Support Sustained Lunar Exploration</h2>

                                <a className="btn animate" tabIndex="0" href="/updates/#sustained-lunar-exploration">
                                    <div className="hover"></div>
                                    <span className="text">LEARN MORE</span>
                                </a>

                            </div>
                            <div className="scrollme" style={{ visibility: 'inherit', opacity: '0.995595', transform: 'translate3d(0px, -38.3353px, 0px)' }}>
                                <svg width="30px" height="20px">
                                    <path stroke="#ffffff" strokeWidth="2px" d="M2.000,5.000 L15.000,18.000 L28.000,5.000 "></path>
                                </svg>
                            </div>
                        </div>
                    </div>

                    <div className="section">
                        <div className="background" data-desktop="/static/images/backgrounds-2022/starship-update/S20_Chopstick_Stack_Desktop.jpg" data-mobile="/static/images/backgrounds-2022/starship-update/S20_Chopstick_Stack_Mobile.jpg" style={{ backgroundImage: 'url(https://www.spacex.com/static/images/backgrounds-2022/starship-update/S20_Chopstick_Stack_Desktop.webp)' }}></div>
                        <div className="section-inner feature" style={{ height: '655px' }}>
                            <div className="inner-left-bottom">


                                <h2 className="animate shadowed">Starship Update</h2>

                                <a className="btn animate" tabIndex="0" href="/updates/#starship-update">
                                    <div className="hover"></div>
                                    <span className="text">LEARN MORE</span>
                                </a>

                            </div>
                            <div className="scrollme" style={{ visibility: 'inherit', opacity: '0.995595', transform: 'translate3d(0px, -38.3353px, 0px)' }}>
                                <svg width="30px" height="20px">
                                    <path stroke="#ffffff" strokeWidth="2px" d="M2.000,5.000 L15.000,18.000 L28.000,5.000 "></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <Footer />
                </div>

                <div id="modal" className="modal" >
                    <div className="modal-transform">
                        <div className="modal-bg"></div>
                        <div className="modal-inner">
                            <div className="youtube-wrapper"></div>
                            <span></span>
                        </div>
                        <a href="true" className="modal-close">
                            {/* <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
                                <line className="line1" fill="none" x1="13" y1="13" x2="33" y2="33" stroke-linecap="square"></line>
                                <line className="line2" fill="none" x1="13" y1="33" x2="33" y2="13" stroke-linecap="square"></line>
                            </svg> */}
                        </a>
                    </div>
                </div>
            </body>
        </>

        // <nav className="navbar navbar-expand-sm navbar-dark">
        //     <div className="container-fluid">
        //         <ul className="navbar-nav">
        //             <a className="navbar-brand" href="true">
        //                 <img src='/assets/images/logo/logo.png' alt='logo' />
        //             </a>
        //             <li className="nav-item">
        //                 <a className="nav-link active" href="true">Active</a>
        //             </li>
        //             <li className="nav-item">
        //                 <a className="nav-link" href="true">Link</a>
        //             </li>
        //             <li className="nav-item">
        //                 <a className="nav-link" href="true">Link</a>
        //             </li>
        //             <li className="nav-item">
        //                 <a className="nav-link disabled" href="true">Disabled</a>
        //             </li>
        //         </ul>
        //     </div>
        // </nav>
    )
}
