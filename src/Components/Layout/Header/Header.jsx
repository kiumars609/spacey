import React, { useState } from 'react'
import Modal from 'react-bootstrap/Modal';
import './style.css'

export default function Header() {

    const [showModal, setShowModal] = useState(false);
    const handleCloseModal = () => setShowModal(false);
    const handleShowModal = () => setShowModal(true);
    return (
        <>
            <div id="header" className="section" data-limit=".15">
                <div className="header-bg"></div>
                <div className="header-inner">
                    <a href="/" id="logo" alt="SpaceX - go to homepage">
                        <img src='/assets/images/logo/logo.png' alt='space-y' />
                    </a>
                    <div id="navigation">
                        <ul className="nav-links">
                            <li className="nav-item"><a href="/vehicles/falcon-9/">Falcon 9</a></li>
                            <li className="nav-item"><a href="/vehicles/falcon-heavy/">Falcon Heavy</a></li>
                            <li className="nav-item"><a href="/vehicles/dragon/">Dragon</a></li>
                            <li className="nav-item"><a href="/vehicles/starship/">Starship</a></li>
                            <li className="nav-item"><a href="/human-spaceflight/">Human Spaceflight</a></li>
                            <li className="nav-item"><a href="/rideshare/">Rideshare</a></li>
                            <li className="nav-item"><a href="https://www.starlink.com" target="_">Starlink</a></li>
                        </ul>
                    </div>
                </div>
                <div id="navigation-right">
                    <ul className="nav-links">
                        <li className="nav-item"><a href="https://shop.spacex.com/">SHOP</a></li>
                    </ul>
                </div>
                <label class="sidebar-toggle" id="hamburger" onClick={handleShowModal}>
                    <div id="bar1" className="bar" style={{ transform: 'matrix(1, 0, 0, 1, 0, 0)' }}></div>
                    <div id="bar2" className="bar" style={{ width: '100%', transform: 'matrix(1, 0, 0, 1, 0, 0)' }}></div>
                    <div id="bar3" className="bar" style={{ transform: 'matrix(1, 0, 0, 1, 0, 0)' }}></div>
                </label>
                <Modal className='modal-right modal-card dark' show={showModal} onHide={handleCloseModal}>
                    <Modal.Header closeButton>
                        {/* <Modal.Title>Modal heading</Modal.Title> */}
                    </Modal.Header>
                    <Modal.Body>
                        <div id="menu-navigation" className='mnu'>
                            <ul className="nav-links">
                                <li className="nav-item primary"><a href="/vehicles/falcon-9/">Falcon 9</a></li>
                                <li className="nav-item primary"><a href="/vehicles/falcon-heavy/">Falcon Heavy</a></li>
                                <li className="nav-item primary"><a href="/vehicles/dragon/">Dragon</a></li>
                                <li className="nav-item primary"><a href="/vehicles/starship/">Starship</a></li>
                                <li className="nav-item primary"><a href="/human-spaceflight/">Human Spaceflight</a></li>
                                <li className="nav-item primary"><a href="/rideshare/">Rideshare</a></li>
                                <li className="nav-item primary"><a href="https://www.starlink.com" target="_">Starlink</a></li>
                                <li className="nav-item secondary"><a href="/mission/">Mission</a></li>
                                <li className="nav-item secondary"><a href="/launches/">Launches</a></li>
                                <li className="nav-item secondary"><a href="/careers/">Careers</a></li>
                                <li className="nav-item secondary"><a href="/updates/">Updates</a></li>
                                <li className="nav-item secondary"><a href="https://shop.spacex.com/">Shop</a></li>
                            </ul>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        </>
    )
}
