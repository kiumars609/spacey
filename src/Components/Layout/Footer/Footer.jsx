import React from 'react'

export default function Footer() {
    return (
        <>
            <div id="footer">
                <p>
                    <span>SpaceX © 2022</span>
                    <a href="https://twitter.com/spacex" className="social">TWITTER</a>
                    <a href="https://www.youtube.com/spacex" className="social">YOUTUBE</a>
                    <a href="https://www.instagram.com/spacex/" className="social">INSTAGRAM</a>
                    <a href="https://www.flickr.com/photos/spacex" className="social">FLICKR</a>
                    <a href="https://www.linkedin.com/company/spacex" className="social">LINKEDIN</a>
                    <a href="/media/privacy_policy_spacex.pdf" target="_blank" className="social">PRIVACY POLICY</a>
                    <a href="/supplier/" className="social">SUPPLIERS</a>
                </p>
            </div>
        </>
    )
}
